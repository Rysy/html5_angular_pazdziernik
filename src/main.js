
var myapp = angular.module('myapp',[
    'income_form'
])

myapp.directive('recordDrop',function(){
  return {
      restrict:'A',
      link: function(scope, $elem, attrs){
        //   console.log($elem[0])
          $elem.on('dragover',function(e){
              e = e.originalEvent;
              e.preventDefault()
              $elem.css('background','yellow')
            })
            
          $elem.on('dragleave',function(e){
              $elem.css('background','inherit')
          })
           
        $elem.on('drop',function(e){
                $elem.css('background','inherit')
                e = e.originalEvent;
                var data = e.dataTransfer.getData("x-application/records")
                data = JSON.parse(data);

                scope.$apply(function(){
                    scope
                    .$eval( attrs.recordDrop,{
                        "$event":data
                    });
                })
            })
      }
  }  
})


myapp.directive('dragabbleRecord',function(){

    return {
        restrict:'A',
        link: function(scope, $elem, attrs){

            $elem.on("dragstart", function(e){
                var record = scope
                    .$eval( attrs.dragabbleRecord);
                    e = e.originalEvent;
                    
                    e.dataTransfer.dropEffect = "move";
                    
                    record =  JSON.stringify(record);

                    e.dataTransfer
                    .setData("text/plain", record);

                    e.dataTransfer
                    .setData("x-application/records",record)
            })


        }
    }
})

myapp.controller('main',function($scope){
    $scope.title = 'Angular App'
    
    $scope.data ={
        placki: 'malinowe'
    }

    var secret = 'nalesniki!'
})

