
// Create new module 
angular.module('income_form', [])

    .factory('IncomeRecords', function ($http, $rootScope) {
        var url = 'http://localhost:3000/records/';

        var service = {
            data: {
                records: [],
                active: null
            },
            saveRecord: function (record) {
                // this.data.records.push(record)
                
                var request = record.id?
                // update:
                    $http.put(url + record.id, record) :
                // create:
                    $http.post(url, record);

                    return request.then(function (xhr) {
                        record = xhr.data;
                        // Wait for nested async operations:
                        return service.fetchRecords()
                    }).then(function () {
                        return record;
                    }).catch(function(err){
                        console.log('err',err)
                        // np. $broadcast ...
                    })
            },
            select: function(record){
                this.data.active = record;
                $rootScope.$broadcast('record-activated',record)
            },
            delete: function (id) {
                $http.delete(url + id)
                    .then(function (xhr) {
                        service.fetchRecords()
                    })
            },
            getData: function () {
                return this.data;
            },
            fetchRecords: function () {
                // Promise chain:
                return $http.get(url)
                    .then(function (xhr) {
                        // console.log(this,xhr.data)
                        service.data.records = xhr.data;
                        return xhr.data;
                    })
            }
        }
        return service;
    })
    // .service('IncomeRecords', IncomeRecords)

    .controller('incomeForm', function ($scope, IncomeRecords, $window) {

        $scope.data = {
            company: '',
            category: '',
            income: 0
        }

        var draft = localStorage.getItem('incomeForm')
        draft = draft && JSON.parse(draft)
        if(draft){
            $scope.data = draft
        }
        $window.addEventListener('storage', function(e){
            $scope.$apply(function(){
                $scope.data = JSON.parse(e.newValue)
            })
        })

        //$scope.$watch('data.company')
        $scope.$watchCollection('data', function(val){
            localStorage.setItem('incomeForm',JSON.stringify(val))
        })

        $scope.$on('record-activated',function(e,record){
            $scope.data = Object.assign({},record)
        })
        

        $scope.reset = function(record){
            IncomeRecords.select(record)
        }

        $scope.save = function () {
            if($scope.formRef.$invalid){ return; }
            
            IncomeRecords.saveRecord($scope.data)
            .then(function(record){
                IncomeRecords.select(record)
            })
        }
    })



