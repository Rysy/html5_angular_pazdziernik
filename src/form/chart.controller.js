angular.module('income_form')
.directive('recordsPlot',function(IncomeRecords){


    return {
        restrict:'E',
        templateUrl:'form/chart.tpl.html',
        link:function(scope,$elem, attr){
            var data = scope.$eval(attr.records)
            update(data)

            scope.$watch(attr.records,function(records){
                update(records)
            })

            $elem.on('click','circle',function(){
                IncomeRecords.select(null)
                console.log($(this).data('id'))
            })

            function update(data){
                      
                var max = data.reduce(function(max,p){
                    return Math.max(max,p)
                },0)
                var min = data.reduce(function(min,p){
                    return Math.min(min,p)
                },0)
                data = data.map(function(p){
                    return {
                        id: p.id,
                        label: p.company,
                        value: p.income
                    }
                })
                scope.data = data
            }


        }
    }
})