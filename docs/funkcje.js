// Hoisting:
function f(a,b,c){
    if(false){
		var z = 1
    }
	console.log(z)
};
f();
z

// Hoisting funkcji

function f(a,b,c){

	function f1(){
		console.log(z)
    }
	var z = 2;
	f1()
};
f();

// Przypisanie funkcji do zmiennej

function f(a,b,c){

	f1()
	var z = 2;

	var f1 = function (){
		console.log(z)
    }
};
f();

// Zwracanie funkcji z funkcji

function f(a,b,c){
	var z = 2;

	return function (){
		console.log(z)
    }
};
f2 = f();

// Map i Filter - przekazywanie funkcji jako argumentu
multply = function(x){ return x*3 } 
getEvens = function(x){ return x % 2 == 0 }

lista2 = lista
		.map( multply )
		.filter( getEvens )


// IIF - samo wywolujaca sie funkcja
(function(){ console.log('hello') })()

// Reduce - dodawanie:
lista.reduce( function(agr, next){ 
	agr += next;
	return agr;
},0)

// Reduce - objekty:
lista
.map( function(x){ return { value: x } })
.reduce( function(agr, next){ 
	agr.sum += next.value;
	return agr;
},{ sum: 0})

// Reduce - grupowanie:
[{ team:'red', score:2}, {team:'blue',score:3},{team:'red', score:2}]
.reduce( function( scores, record ){
    // add new teams
    scores[ record.team ] = scores[ record.team ] || 0;
    // add scores
	scores[ record.team ] += record.score
},{
	red: 0, blue: 0
})
// reduce - zbieranie do tablic
[{ team:'red', score:2}, {team:'blue',score:3},{team:'red', score:2}]
.reduce( function( scores, record ){
    scores[ record.team ] = scores[ record.team ] || [];
	scores[ record.team ].push( record );
	return scores;
},{
	red: [], blue: []
})

