// Proste tworzenie obiektow:

function makePerson(name){
	return {
    	name: name,
		sayHello: function(){ return 'hello Iam ' + this.name }
	}
}
a = makePerson('Alice');
b = makePerson('Bob')
// {name: "Bob", sayHello: ƒ}
a.sayHello()
// "hello Iam Alice"
b.sayHello()
// "hello Iam Bob"
var robot = { name:'robot', run: a.sayHello }
// undefined
a.sayHello()
// "hello Iam Alice"
robot.run()
// "hello Iam robot"

/* 
    Wspoldzielenie metod obiektow
*/
var greeter = function(){ return 'hello Iam ' + this.name }

function makePerson(name){
	return {
    	name: name,
		sayHello: greeter
	}
}
a = makePerson('Alice');
b = makePerson('Bob')
// {name: "Bob", sayHello: ƒ}
a.sayHello()
// "hello Iam Alice"
a.sayHello()
// "hello Iam Alice"
a.sayHello == b.sayHello
// true
greeter
// ƒ (){ return 'hello Iam ' + this.name }
greeter()
// "hello Iam "
window.greeter()
// "hello Iam "
window.name = "Okienko"
// "Okienko"
window.greeter()
// "hello Iam Okienko"
greeter()
// "hello Iam Okienko"

/* 
    Tworzenie obiektow z new, this i prototype
*/

function Person(name){
	this.name = name
}
Person.prototype.sayHello = function(){ return 'hello Iam ' + this.name }

a = new Person('Alice');
b = new Person('Bob')
// Person {name: "Bob"}
a.sayHello()
// "hello Iam Alice"
a.sayHello == b.sayHello
// true
a
// Person {name: "Alice"}
//  name: "Alice"
//  __proto__: 
//      sayHello: ƒ ()
//      constructor: ƒ Person(name)
a instanceof Person
// true

/* 
    Dziedziczenie prototypowe
*/
function Person(name){
	this.name = name
}
Person.prototype.sayHello = function(){ return 'hello Iam ' + this.name }

function Employee(name,salary){
	Person.call(this,name)
	this.salary = salary;
}
Employee.prototype = Object.create(Person.prototype)
Employee.prototype.work = function(){ return 'where is my '+this.salary+'!?' }

bob = new Employee('Bob',1200)
// Employee {name: "Bob", salary: 1200}
//  name:"Bob"
//  salary:1200
//  __proto__:Person
//      work:ƒ ()
//      __proto__:
//          sayHello: ƒ ()
//          constructor:ƒ Person(name)
bob.sayHello()
// "hello Iam Bob"
bob.work()
// "where is my 1200!?"
bob instanceof Employee
// true 
bob instanceof Person 
// true 

// Łańcuch prototypow:
var div = document.createElement('div')
div.__proto__.__proto__.__proto__.__proto__.__proto__.__proto__

/* 
    EcmaScript2015 - najnowszy standard JS
        - tylko najnowsze przeglądarki
        - lub "transpilacja" np. BabelJS + gulp-babel ;-)
    https://babeljs.io/
*/
class Person{
    constructor(name) { this.name = name }
    sayHello(){ return 'hi '+this.name  }
}
a = new Person('Anna')

class Employee extends Person{

    constructor(name,salary) {  
        super(name); 
        this.salary = salary
    }
    work(){ 
        return 'please, my '+this.salary;
    }
}
tom = new Employee('tom',1200)
tom.work()